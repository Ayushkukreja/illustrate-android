package com.example.illustrate

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import java.util.ArrayList

class DrawingBoardView(context: Context, attrs: AttributeSet): View(context, attrs) {

    private var drawingPath: CustomPath? = null
    private var canvas: Canvas? = null
    private var canvasBitmap: Bitmap? = null
    private var drawingPaint: Paint? = null
    private var canvasPaint: Paint? = null
    private var brushSize: Float = 0.0F
    private var color = Color.BLACK

    private var pathList = ArrayList<CustomPath>()

    init {
        setUpDrawing()
    }

    private fun setUpDrawing(){
        drawingPaint =  Paint()

        drawingPaint!!.color = color
        drawingPaint!!.style = Paint.Style.STROKE
        drawingPaint!!.strokeJoin = Paint.Join.ROUND
        drawingPaint!!.strokeCap = Paint.Cap.ROUND

        brushSize = 20F
        canvasPaint = Paint()
        drawingPath = CustomPath(color, brushSize)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        canvasBitmap = Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888)

        canvas = Canvas(canvasBitmap!!)
    }
    // it will call on every pixel
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawBitmap(canvasBitmap!!, 0F,0F,canvasPaint)



        if (! drawingPath!!.isEmpty){
            drawingPaint!!.strokeWidth = drawingPath!!.brushThickness
            drawingPaint!!.color = drawingPath!!.color
            canvas?.drawPath(drawingPath!!, drawingPaint!!)
        }

        for(path in pathList){
            drawingPaint!!.strokeWidth = path.brushThickness
            drawingPaint!!.color = path.color
            canvas?.drawPath(path, drawingPaint!!)
        }
    }


    //called when touced event created
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val TouchX = event?.x
        val TouchY = event?.y

        when(event?.action){
            MotionEvent.ACTION_DOWN ->{
                drawingPath!!.color = color
                drawingPath!!.brushThickness = brushSize

                drawingPath!!.reset()

                drawingPath!!.moveTo(TouchX!!, TouchY!!)
//                if(TouchX != null && TouchY != null){
//                    drawingPath!!.moveTo(TouchX, TouchY)
//                }
            }
            MotionEvent.ACTION_MOVE ->{
                if(TouchX != null && TouchY != null){
                    drawingPath!!.lineTo(TouchX, TouchY)
                }
            }

            MotionEvent.ACTION_UP ->{
                pathList .add(drawingPath!!)
                drawingPath = CustomPath(color, brushSize)
            }
            else -> return false
        }
        invalidate()
        return true
    }

    fun setBrushSize(newBrushSize: Float){
        brushSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newBrushSize,resources.displayMetrics)
        drawingPaint!!.strokeWidth = brushSize
    }

    fun setColor(newColor: Int){
        color = newColor
        drawingPaint!!.color = newColor

    }

    internal inner class CustomPath(var color: Int, var brushThickness: Float): Path(){

    }
}